stegsnow (20130616-8) unstable; urgency=medium

  * Add missing include <string.h> (Closes: #1066597)
  * Bump Standards-Version to 4.7.0

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 14 May 2024 22:19:18 +0700

stegsnow (20130616-7) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * debian/control: bumped Standards-Version to 4.6.1.
  * debian/copyright:
      - Converted the last paragraph of the Apache-2.0 in a comment.
      - Updated packaging copyright years.
  * debian/patches/10-man.patch: added field 'Forwarded'.
  * debian/upstream/metadata: created.
  * debian/watch:
      - Changed from filenamemangle to dversionmangle.
      - Using a secure URI.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from old 12 to 13.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 22 Nov 2022 23:18:12 -0300

stegsnow (20130616-6) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * debian/copyright: updated packaging copyright years.

  [ Samuel Henrique ]
  * Configure git-buildpackage for Debian.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 07 Apr 2020 00:12:47 -0300

stegsnow (20130616-5) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.5.0.
  * debian/copyright:
      - Added packaging rights for Samuel Henrique.
      - Updated packaging copyright years.

  [ Samuel Henrique ]
  * Add salsa-ci.yml.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 07 Feb 2020 09:43:27 -0300

stegsnow (20130616-4) unstable; urgency=medium

  * debian/copyright:
      - Changed packaging licensing from GPL-2+ to BSD-3-Clause.
        (Closes: #920484)
      - Updated the Apache-2.0 license text.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 06 Feb 2019 09:05:16 -0200

stegsnow (20130616-3) unstable; urgency=medium

  * New maintainer and co-maintainer. (Closes: #920098)
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Bumped Standards-Version to 4.3.0.
      - Improved long description.
      - Set VCS fields to use Salsa.
  * debian/copyright:
      - Reorganized the file.
      - Updated upstream and packaging copyright years.
  * debian/debian-vars.mk: removed.
  * debian/patches/10-man.patch: updated header and added Last-Update field.
  * debian/repack.sh: removed.
  * debian/rules: reorganized the file content.
  * debian/tests/*: added to provide trivial tests.
  * debian/watch:
      - Bumped version to 4.
      - Improved the rule.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 25 Jan 2019 00:52:30 -0200

stegsnow (20130616-2) unstable; urgency=medium

  * debian/control
    - (Homepage): Update.
    - (Standards-Version): Update to 3.9.8.
    - (Vcs-*): Update to anonscm.debian.org.
  * debian/copyright
    - Update URLs.

 -- Jari Aalto <jari.aalto@cante.net>  Thu, 20 Oct 2016 12:34:26 +0300

stegsnow (20130616-1) unstable; urgency=low

  * Initial release (Closes: #293244).

 -- Jari Aalto <jari.aalto@cante.net>  Wed, 26 Jun 2013 11:36:40 +0300
